#!/usr/bin/env bash
#
# Script clones all Git repositories from the GitHub server
#
# It requires following env vars to be defined:
#   * GITHUB_USERNAME
#   * GITHUB_PASSWORD - if using 2FA, generate a new personal access token
#       at https://github.com/settings/tokens and use it instead of password,
#       see: https://developer.github.com/v3/auth/#working-with-two-factor-authentication
#   * GITHUB_ORGANISATION_NAME
#
# Optional env vars:
#   * GITHUB_SERVER - if using custom GitHub EE server rather than github.com
#        should have format: [hostname]/api/v3,
#        see https://developer.github.com/enterprise/2.17/v3/#root-endpoint
#   * URL_ATTR - choose type of URLs to use
#        clone_url - HTTPS
#        ssh_url - SSH (default)
#

set -eu


#
# Vars
#

GITHUB_SERVER=${GITHUB_SERVER:-api.github.com}

GITHUB_API_URL=https://${GITHUB_USERNAME}:${GITHUB_PASSWORD}@${GITHUB_SERVER}

URL_ATTR=${URL_ATTR:-ssh_url}



#
# Main
#

curl -s ${GITHUB_API_URL}/orgs/${GITHUB_ORGANISATION_NAME}/repos?per_page=200 |\
    jq ".[] | select(.archived==false) | .${URL_ATTR}" |\
    xargs -n 1 git clone
