#!/usr/bin/env bash
#
# Script to extract a subdir from one git repo with history and move it to a subdir in another Git repo
#

set -eu

#
# Input, use absolute paths
#

SOURCE_REPO=$(pwd)/microsoft/vsts/misc
TARGET_REPO=$(pwd)/git/migrations
PREFIX_SOURCE=Migrate_Git_to_Git
PREFIX_TARGET=git_to_git


#
# Internal vars
#

TEMP_BRANCH=split

TEMP_REPO=`basename $0`
TEMP_REPO=/tmp/${TEMP_REPO%.sh}-`date +'%Y%m%d_%H%M%S'`

#
# Main
#

echo "--- Extracting a subtree in source repo as a branch ..."
pushd $SOURCE_REPO
git subtree split -b $TEMP_BRANCH --prefix=$PREFIX_SOURCE
popd
echo

echo "--- Creating temp repo and pulling there a split branch from source repo ..."
git init $TEMP_REPO
pushd $TEMP_REPO
git pull $SOURCE_REPO ${TEMP_BRANCH}:master
popd
echo

echo "--- Pulling temp repo into target repo under a target dir ..."
pushd $TARGET_REPO
git subtree add --prefix=$PREFIX_TARGET $TEMP_REPO HEAD
popd
echo

echo "--- Cleanup: deleting temp repo ..."
rm -fr $TEMP_REPO
echo

echo "--- Cleanup: deleting split branch in source repo ..."
pushd $SOURCE_REPO
git branch -D $TEMP_BRANCH
popd
