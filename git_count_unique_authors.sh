#!/usr/bin/env bash

set -eu


# authors - current branch
git log --format='%aN %aE' | sort | uniq -c

# authors - all branches
git log -all --format='%aN %aE' | sort | uniq -c

# committers - current branch
git log --format='%cN %cE' | sort | uniq -c

# committers - all branches
git log -all --format='%cN %cE' | sort | uniq -c


# simpler way, but only for current branch
#   - authors
git shortlog -s -n -e

#   - commiters
git shortlog -s -n -e -c
