#!/usr/bin/env python3
"""
Script recursively searches for git repositories in a directory,
and runs git push and pull for them
"""

import argparse
import glob
import os
from pathlib import Path
from subprocess import check_output


def parse_args():
    """Parse command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Batch replace remotes in git repositories')

    parser.add_argument(
        '--root-dir', default=os.getcwd(),
        help='Script searches for git repos in all child dirs of the root-dir')

    parser.add_argument(
        '--skip-push', action='store_true', help="Skip git push")

    parser.add_argument(
        '--verbose', action='store_true', help="Verbose output")

    parser.add_argument(
        '--dry-run', action='store_true', help="Dry run, no changes done")

    return parser.parse_args()


def process_repo(git_repo_dir):
    """Process dir as a git repo"""

    print('\033[92m' + f"Repo: {git_repo_dir}" + '\x1b[0m')

    call_dir = Path.cwd()
    os.chdir(git_repo_dir)

    if ARGS.dry_run:
        print("\t skipping actions as in dry-run mode")
    else:
        check_output(["git", "pull", "--rebase"]).decode("utf-8")
        if not ARGS.skip_push:
            check_output(["git", "push"]).decode("utf-8")

    os.chdir(str(call_dir.absolute()))


def process_dir(current_dir):
    """Process dir - find out if it's a git repo or just a dir"""

    if ARGS.verbose:
        print(f"Dir: {current_dir}")
    if os.path.isdir(os.path.join(current_dir, '.git')):
        process_repo(current_dir)
    else:
        for subdir in glob.glob(os.path.join(current_dir, '*')):
            if os.path.isdir(subdir):
                process_dir(subdir)


#
# Main
#

if __name__ == '__main__':
    ARGS = parse_args()

    process_dir(Path(ARGS.root_dir))
