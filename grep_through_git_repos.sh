#!/usr/bin/env bash
#
# Script greps for a string in the content of git repositories in a directory.
# You can use blob patterns to limit the search in specific branches and files.
#
# Examples of use:
#
#   $ ./grep_through_git_repos.sh git@github.com:angular/angular.git
#
#   $ BASE_DIR=~/git BRANCH_SPEC=* FILE_SPEC=Jenkinsfile \
#       ./grep_through_git_repos.sh git@github.com:org123/
#

set -eu


#
# Parameters
#

# Root directory for search
BASE_DIR=${BASE_DIR:-.}

# In which branches to search, examples: master, *, refs/heads/rel-*
BRANCH_SPEC=${BRANCH_SPEC:-refs/heads/master}

# In which files to search, examples: .gitmodules, Jenkinsfile, *.yml
FILE_SPEC=${FILE_SPEC:-.gitmodules}

# Search string, required parameter
SEARCH_REGEX=$1


#
# Main
#

for repo in `find $BASE_DIR -type d -name .git`
do
	echo "Repo: $repo"
	pushd $repo/.. > /dev/null

	for branch in `git for-each-ref $BRANCH_SPEC --format="%(refname)"`
	do
		echo -e "\t Branch: $branch"
		found=0

		for blob in `git ls-tree -r $branch -- $FILE_SPEC | awk '{print $3}'`
		do
			# "true" below is important not to fail the script as there is `set -e` defined
			git grep -E --ignore-case -q $SEARCH_REGEX $blob && let "found++" || true
		done

		if [ $found -gt 0 ]
		then
			echo -e "\t\t found $found matches"
		fi
	done

	popd > /dev/null
done
