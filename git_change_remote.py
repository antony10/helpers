#!/usr/bin/env python3
"""
Script updates remote URLs for local git repositories,
for example when:
    - remote Git server has been renamed
    - repository has been moved
It searches for repositories recursively in a specified directory.

First use with "--dry-run" option,
and if output is what you expect, run again without this option.

*Examples*

  * when cloned with SSH:
    $ ./git_change_remote.py \
        --old-remote git@github.com:org1/ \
        --new-remote git@github.com:org2/ \
        --dry-run

  * when cloned with HTTPS:
    $ ./git_change_remote.py \
        --old-remote https://github.com/org1/ \
        --new-remote https://github.com/org2/ \
        --dry-run

The URL match is case-insensitive.
If remote does not contain the old URL script ignores it,
so repos cloned from other sources are not touched.
It also quietly ignores repos without remotes.

Warning:
    make sure new remote doesn't contain old remote name,
    otherwise operation will not be idempotent.


*Submodules*

Script searches for any git submodules inside a repository,
and warns if they also use old Git remote, suggesting to make them relative.
But it doesn't actually change submodules as this would generate a new commit,
which is not always desired.

To manually convert submodules to be relative, follow this:
  * edit .gitmodules file changing absolute URL to relative
    (you'll need to figure out if any '../' are required)
  * $ git submodule sync
  * $ git submodule update --init --recursive --remote
  * $ git add .gitmodules
  * $ git commit -m "Made submodules relative"
  * $ git push
  * rerun the script again to make sure there are no more WARNINGs


*References*
    * https://git-scm.com/docs/git-remote#git-remote-emset-urlem
    * https://git-scm.com/docs/git-submodule
    * https://docs.gitlab.com/ce/ci/git_submodules.html
"""

import os
from pathlib import Path
from subprocess import check_output
import argparse
import re


def parse_args():
    """Parse command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Batch replace remotes in git repositories')

    parser.add_argument(
        '--old-remote-base',
        required=True,
        help='Old remote url base to be replaced')

    parser.add_argument(
        '--new-remote-base',
        required=True,
        help='New remote url base to be set for repositories')

    parser.add_argument(
        '--root-dir',
        default=os.getcwd(),
        help='Script will search for git repos recursively in this root dir')

    parser.add_argument(
        '--dry-run', action='store_true',
        default=False,
        help='Search for the repositories, but do not change anything')

    args = parser.parse_args()

    args.old_remote_base = args.old_remote_base.lower()
    args.new_remote_base = args.new_remote_base.lower()

    return args


def process_submodules(git_repo_dir):
    """Searches for submodules that reference old remote, and warns"""

    submodules_file = '.gitmodules'

    if (git_repo_dir / submodules_file).exists():
        with open(git_repo_dir / submodules_file, 'r') as file:
            for line in file:
                match = re.match(line, r'url\s*=\s*(.+)\s*$')
                if match:
                    if ARGS.old_remote_base in line:
                        print("Make submodule relative:", match.group(1))


def process_repo(git_repo_dir):
    """
    See if specified git repo references old remote,
    and change them to new remote
    """

    print(f"Repo: {git_repo_dir}")

    call_dir = Path.cwd()
    os.chdir(str(git_repo_dir.absolute()))

    all_remotes = [r.strip() for r in
                   check_output(["git", "remote"]).decode("utf-8").split('\n')
                   if len(r) > 0]

    for remote in all_remotes:
        remote_url = check_output(["git", "remote", "get-url", remote]).\
            decode("utf-8").strip()

        if remote_url.startswith(ARGS.old_remote_base):
            new_remote_url = remote_url.replace(ARGS.old_remote_base,
                                                ARGS.new_remote_base)

            if ARGS.dry_run:
                print(f"\t Dry-run: not changing {remote} :"
                      f" {remote_url} -> {new_remote_url}")
            else:
                print(f"\t Changing {remote} :"
                      f" {remote_url} -> {new_remote_url}")
                check_output(["git", "remote", "set-url",
                              remote, new_remote_url]).decode("utf-8")

        elif remote_url.startswith(ARGS.new_remote_base):
            print(f"\t remote is already updated: {remote} : {remote_url}")

        else:
            print(f"\t Ignoring unknown remote: {remote} : {remote_url}")

    process_submodules(git_repo_dir)

    os.chdir(str(call_dir.absolute()))


def process_dir(current_dir):
    """
    If dir is a git repo - process its remotes,
    otherwise traverse it recursively
    """

    if (current_dir / '.git').exists():
        process_repo(current_dir)
    else:
        for file in current_dir.glob('*'):
            if (current_dir / file).is_dir():
                process_dir(current_dir / file)


#
# Main
#

if __name__ == '__main__':
    ARGS = parse_args()

    process_dir(Path(ARGS.root_dir))
